# -*- coding: utf-8 -*-
"""
Data Preprocessing Template
Created on Wed Mar 21 23:24:00 2018
@author: Reinaldo Felipe Soares Araujo
"""

import numpy as np
import matplotlib.pyplot as pltImputer
import pandas as pd

dataset = pd.read_csv('Data.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 3].values

# Library for pre-processing data - taking care of missing data
from  sklearn.preprocessing import Imputer
imputer = Imputer(missing_values = 'NaN', strategy = 'mean', axis = 0)
imputer = imputer.fit(X[:,1:3])
X[:,1:3] = imputer.transform(X[:,1:3])

# Encoding categorigal data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X = LabelEncoder()
X[:,0] = labelencoder_X.fit_transform(X[:,0])

labelencoder_y = LabelEncoder()
y = labelencoder_y.fit_transform(y)

#It's not correct to represent the countries by numbers, because it can fail
# the equation. French isn't greater than another country as example

onehoteconder =  OneHotEncoder(categorical_features = [0])
X = onehoteconder.fit_transform(X).toarray()

#split data between training and data
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

#Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)



